**Title:** _"SAML SSO Boot Camp - **your-name**"_

**Goal of this checklist:** Set a clear path for SAML SSO Expert training

**Objectives**: At the end of this bootcamp, you should be able to:
* Understand how GitLab leverages the OmniAuth gem with a SAML Strategy to act as a SAML 2.0 Service Provider
* Understand how to set up SAML and SCIM apps for SSO for Groups
* Troubleshoot customer's issues with SAML and SCIM

Remember to contribute to any documentation that needs updating

### Stage 1: Commit and Become familiar with what SAML is

- [ ] **Done with Stage 1**

1. [ ] Ping your manager on the issue to notify them you have started.
1. [ ] Commit to this by adding yourself to the [knowledge areas page](https://about.gitlab.com/handbook/support/workflows/knowledge_areas.html).
1. [ ] Commit to this by notifying the current experts that they can start routing non-technical SAML SSO questions to you
1. [ ] Read through the [GitLab SAML Documentation](https://docs.gitlab.com/ee/integration/saml.html).
1. [ ] Read through the [omniauth-saml gem documentation](https://github.com/omniauth/omniauth-saml).
1. [ ] Read through the [GitLab SAML SSO for Groups Documentation](https://docs.gitlab.com/ee/user/group/saml_sso/).
1. [ ] Read through the [GitLab SCIM provisioning using SAML SSO for Groups Documentation](https://docs.gitlab.com/ee/user/group/saml_sso/scim_setup.html).
1. [ ] Watch the [Manage 201 SAML knowledge sharing](https://www.youtube.com/watch?v=CW0SujsABrs). You can access the [slides](https://docs.google.com/presentation/d/10uUGoDB4nN0Ho42vwq3-aTO5UMO5a3Bjgor8maWe-zw/edit#slide=id.g444cb56ba0_0_0) as well.
1. [ ] Watch the Support Authentication Deep Dive (recorded June 2020) and review the accompanied slides:
      - [ ] [Session 1 of Deep Dive](https://drive.google.com/file/d/16hDb4lHXril_1UmchI5_NlH8iN_b6Kdl/view?usp=sharing)
      - [ ] [Session 2 of Deep Dive](https://drive.google.com/file/d/1nNFX-v1AvCaoibDrcw57jxD59BR-sT8Z/view?usp=sharing)
      - [ ] [Deep Dive Slides](https://docs.google.com/presentation/d/1S8IrmKBLMOsSxEJNQHBLja1ax3qZ0YFicUBM_RFtvVg/edit?usp=sharing)

### Stage 2: Technical Setup

- [ ] **Done with Stage 2**

1. Implement SAML
   - Note: If using GDK, follow the [SAML How To Documentation](https://gitlab.com/gitlab-org/gitlab-development-kit/blob/master/doc/howto/saml.md). If you prefer, you can use the same Docker images but with a non-GDK instance of GitLab.
   1. [ ] Set up instance-wide SAML on your GitLab instance.
   1. [ ] Set up Group SAML on your GitLab instance.
   1. [ ] Contribute to the documentation with any issues or troubleshooting steps.

1. Create a test app on a cloud provider IdP where we support SCIM and connect it either with your GDK or a GitLab.com group.
    1. [ ] The [infrastructre for troubleshooting workflow page](https://about.gitlab.com/handbook/support/workflows/test_env.html#azure-testing-environment) has info on getting access to Azure or Okta. Alternatively, you can create a trial account on one of the platforms. As most questions are about Azure, consider choosing Azure for this exercise.
    1. [ ] If using Gitlab.com, ensure your GitLab.com test group has Silver or Gold plan. If needed, create an access request to get a test group upgraded, or ask team members for access to an existing one.
    1. [ ] Follow the documentation to set up SAML and SCIM. The [specific provider sections](https://docs.gitlab.com/ee/user/group/saml_sso/#providers) link to a demo video if one exists. If we have SCIM for an IdP where no such video exists, consider contributing one!
    1. [ ] Ensure you are familiar with the [troubleshooting section](https://docs.gitlab.com/ee/user/group/saml_sso/#troubleshooting) in order to know what common cases are documented.

### Stage 3: Tickets

- [ ] **Done with Stage 3**

1. [ ] Go through 10 solved SAML/SSO tickets to check the responses and get a sense
of the types of frequently asked questions that come up.
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
1. [ ] Answer 10 SAML/SSO tickets and paste the links here, even if a ticket seems
too advanced for you to answer. Find the answers from an expert and relay them to
the customers.
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __

### Stage 4: Pair on Customer Calls

- [ ] **Done with Stage 4**

1. [ ] Pair on two calls, where a customer has a problem with SAML/SSO.
   1. [ ] call with ___
   1. [ ] call with ___

### Penultimate Stage: Review
You feel that you can now do all of the objectives:
1. [ ] Understand how GitLab leverages the OmniAuth gem with a SAML Strategy to act as a SAML 2.0 Service Provider.
1. [ ] Understand how to set up SAML and SCIM apps for SSO for Groups.
1. [ ] Troubleshoot customer's issues with SAML/SSO.

Any updates or improvements needed? If there are any dead links, out of date or inaccurate content, missing content whether in this bootcamp or in other documentation, list it below as tasks for yourself!
* [ ] Update ...

### Final Stage

- [ ] Have your trainer and manager review this issue.
- [ ] Manager: schedule a call (or integrate into 1:1) to review how the bootcamp went once you have reviewed this issue.
- [ ] Send a MR to declare yourself a **SAML SSO Expert** on the team page
