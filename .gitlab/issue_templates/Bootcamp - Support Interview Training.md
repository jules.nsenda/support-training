So - you want to help in the hiring process! That's awesome. You (or your manager)
should open one of these issues shortly after you complete your onboarding. Not _everyone_ has
to do the technical interviews, but we definitely want everyone to be involved reviewing assessments.

- [ ] Add your manager as an assignee in this issue.

### Stage One: Assessment Review
- [ ] Review the assessment questions and answers in our [hiring process](https://gitlab.com/gitlab-com/people-group/hiring-processes/tree/master/Engineering/Support).
- [ ] Pair with someone while they review assessments and talk through what sticks out to them. Feel free to reach out in `#support_team-chat` and ask for someone to walk you through the process.
- [ ] Reverse pair with someone on an assessment review, talk them through what sticks out to you.
- [ ] Follow up in the process of the assessments you've done in pairing who passed and reflect:
   - Were they hired?
   - Were any of the notes you provided covered by future interviews?
   - Is there anything you need to be sensitive in for future reviews?
- [ ] Make a merge request on the [Support Engineering Interview Process](https://gitlab.com/gitlab-com/people-group/hiring-processes/blob/master/Engineering/Support/Support%20Engineer/Interview%20Process.md) to add yourself to the **Assessment** section and assign it to your manager. Make sure to ping [your region's technical recruiter](https://gitlab.com/gitlab-com/people-group/hiring-processes/-/blob/master/Engineering/Support/Support%20Engineer/Interview%20Process.md#peopleops-hiring-team) in the MR so that they are informed.


### Stage Two: Technical Interviews
- [ ] Review the [technical interview process](https://gitlab.com/gitlab-com/support/tech-interview/se-interview) and do a dry-run in creating the required resources.
- [ ] Open a [People Ops Interview Training](https://gitlab.com/gitlab-com/people-group/Training/issues/new?issuable_template=interview_training), make a merge request on the [Support Engineering Interview Process](https://gitlab.com/gitlab-com/people-group/hiring-processes/blob/master/Engineering/Support/Support%20Engineer/Interview%20Process.md) to add yourself to the **Interviewers with open interview trainings (tag in to shadow)** section and assign it to your manager for approval.
- [ ] Once your People Ops Interview Trainig is complete: make a merge request on the [Support Engineering Interview Process](https://gitlab.com/gitlab-com/people-group/hiring-processes/blob/master/Engineering/Support/Support%20Engineer/Interview%20Process.md) to add yourself to the **Interviews** section and assign it to your manager. Make sure to ping [your region's technical recruiter](https://gitlab.com/gitlab-com/people-group/hiring-processes/-/blob/master/Engineering/Support/Support%20Engineer/Interview%20Process.md#peopleops-hiring-team) in the MR so that they are informed.

### Additional notes :

- Avoid informing the candidate of your decision, simply point out that our recruitment department will update them shortly.
> "I'll capture what we did here today in my notes, and someone from recruiting will get back to you with next steps as soon as they're able."

/label ~bootcamp ~"interview training"