---
module: Git and GitLab Basics
area: Understanding Technologies
level: Beginner
maintainer: vijirao
pathways:
  support-engineer-onboarding:
    position: 2
  support-manager-onboarding:
    position: 3
---

## Introduction

- We use Git and GitLab to build GitLab! Becoming familiar with the core tools is the first step to being an effective Support team member.
- We'll also introduce you to the GitLab product in this module.

**Goals of this checklist**

At the end of the checklist, you will:
- be equipped with the necessary technical baseline knowledge to understand our products and services.

**General Timeline and Expectations** 

- Read about our [Support Onboarding process](https://about.gitlab.com/handbook/support/training/); the page also shows you the different modules you'll need to complete as part of your Onboarding.
- This issue should take you less than **1 week to complete**.

## Stage 0: Git & GitLab Basics

If you are already comfortable with using Git, and you are able to retain a good amount of information by just watching or reading through, go for it! But if you see a topic that is completely new to you, stop the video and try it out for yourself before continuing.

1. [ ] Go over these topics in [GitLab University](https://docs.gitlab.com/ee/university/):
   1. Under the topic of [Version Control and Git](https://docs.gitlab.com/ee/university/#1-1-version-control-and-git)
      1. [ ] [About Version Control](https://docs.google.com/presentation/d/16sX7hUrCZyOFbpvnrAFrg6tVO5_yT98IgdAqOmXwBho/edit#slide=id.gd69537a19_0_14)
      1. [ ] [Try Git](https://www.katacoda.com/courses/git)
      1. [ ] Explore [Git internals](https://git-scm.com/book/en/v2/Git-Internals-Plumbing-and-Porcelain) and go back to it from time to time to learn more about how Git works
1. Under the topic of [GitLab Basics](https://docs.gitlab.com/ee/university/#12-gitlab-basics)
   1. [ ] All the [GitLab Basics](http://docs.gitlab.com/ee/gitlab-basics/README.html) with which you don't feel comfortable. If you get stuck, see the linked videos under GitLab Basics in GitLab University
      1. [ ] [GitLab Flow](https://www.youtube.com/watch?v=UGotqAUACZA)
      1. [ ] Read about [GitLab releases](https://about.gitlab.com/handbook/engineering/releases/#overview-and-terminology)
      1. [ ] Take a look at how the different GitLab versions compare
         1. [ ] [Feature list](https://about.gitlab.com/features/) - lists all features and which version each is available in
         1. [ ] [GitLab.com feature comparison](https://about.gitlab.com/pricing/gitlab-com/feature-comparison/)
         1. [ ] [Self-managed feature comparison](https://about.gitlab.com/pricing/self-managed/feature-comparison/)
         1. [ ] [Choosing between Self-managed and GitLab.com (SaaS)](https://www.youtube.com/watch?v=7GxO6qvATwU)
   1. Any of these that you don't feel comfortable with in the [user training](https://gitlab.com/gitlab-org/gitlab-ee/tree/master/doc/university/training/topics) we use for our customers.
      1. [ ] `env_setup.md`
      1. [ ] `feature_branching.md`
      1. [ ] `explore_gitlab.md`
      1. [ ] `stash.md`
      1. [ ] `git_log.md`
      1. [ ] For the rest of the topics in `user training`, do a quick read over the file names so you start remembering where to find them.

### Stage 1: GitLab Services & Product Stages

1. [ ] Get familiar with the [different teams in-charge of every stage in the DevOps cycle](https://about.gitlab.com/handbook/product/categories/#devops-stages) and for what they are responsible. This will help you add the right labels when creating issues and escalate in the right Slack channel.
1. [ ] Get familiar with the services GitLab offers on the [pricing page](https://about.gitlab.com/pricing/). GitLab.com includes Free, Bronze, Silver, Gold. These correspond with the self-managed plans of Core, Starter, Premium, Ultimate.  When a customer clicks a plan, they will be given the option of SaaS (GitLab.com) or Self-Managed.

#### Congratulations! You made it, and now have a baseline knowledge of Git, GitLab and its services!

You are now ready to continue on your onboarding path to tackle the next module in line, check our [Support Training](https://about.gitlab.com/handbook/support/training/) page or your `New Support Team Member Start Here` issue for details!

If you think of any improvements to this module, please submit an MR! The file is located in an issue template in the ['support-training` repository](https://gitlab.com/gitlab-com/support/support-training/blob/master/.gitlab/issue_templates).

### Optional: Intermediate GitLab Topics

Discuss with your manager if you should stop here and close your issue or continue. Also, discuss which of these more advanced topics should be followed. Do not do all of them as they might not be relevant to what customers need right now and can be a significant time investment.

You can use GitLab.com to understand the features from an end-user perspective and then use your test instance (if you have one already setup) to understand the configuration of the feature from an Administrative perspective.

- [ ] Get to know the [GitLab API](https://docs.gitlab.com/ee/api/README.html), its capabilities and shortcomings.
- [ ] Learn about [GitLab CI](https://docs.gitlab.com/ee/ci/quick_start/README.html)
- [ ] Create your first [GitLab Page](https://docs.gitlab.com/ee/administration/pages/index.html)
- [ ] Get to know [GitLab through the Rails Console](https://docs.gitlab.com/ee/administration/troubleshooting/navigating_gitlab_via_rails_console.html)
- [ ] [Advanced GitLab debugging and log collection](/gitlab-com/support/support-team-meta/-/issues/1900#5-try-something-a-little-more-advanced)

/label ~onboarding
