<!--

**Title:** *"Customers Console Bootcamp - **your-name**"*

-->

### Overview

Your manager's approval _must_ be obtained before starting this bootcamp. 

Requirements:

- [ ] Basic Ruby, Ruby on Rails and [Ruby Interactive Shell](https://www.digitalocean.com/community/tutorials/how-to-use-irb-to-explore-ruby) knowledge are minimum requirements as all work is done in production Rails console environments.
- [ ] Completion of [Growth training](https://gitlab.com/gitlab-com/support/support-training/-/blob/master/.gitlab/issue_templates/Bootcamp%20-%20Growth.md).

**Goal**: This mini-bootcamp is meant to provide guidance on getting started with customers portal console.

**Objectives**: At the end of this bootcamp, you should be able to:

- follow console related requests.
- resolve Support Engineer Escalations related to Customers Portal through common console related requests.

Start with Stage 1. Stages 2 and 3 can be done simulatenously, **but first**:

1. [ ] After obtaining approval, assign yourself and your trainer to this issue.
1. [ ] Open an [access request](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/issues/new?issuable_template=Single_Person_Access_Request) to request access to `ssh GitLab Customer Server`.
  1. [ ] Make note of [the instructions](https://gitlab.com/gitlab-org/customers-gitlab-com#accessing-production-as-an-admin-and-logs-and-console) for accessing the customers portal production console.
1. [ ] Optional: Set milestones, if applicable, and a due date to help motivate yourself!

### Stage 1: Get primed and ready

- [ ] **Done with Stage 1**

1. [ ] Have an existing Owner add you to the `gitlab-com/support/customers-console` group as an `Owner` so that you are a direct member.
1. [ ] Subscribe to new and/or merge merge requests for [support console project](https://gitlab.com/gitlab-com/support/toolbox/console-training-wheels).
1. [ ] Subscribe to the following labels (if not subscribed to dotcom-internal project):
    1. [`Console Escalation::Customers` label](https://gitlab.com/gitlab-com/support/internal-requests/-/labels?utf8=%E2%9C%93&subscribed=&search=customers+console).

### Stage 2: Related reading and resources


- [ ] **Done with Stage 2**

1. Review these resources:
    1. [ ] [Customers console function list](https://about.gitlab.com/handbook/support/workflows/customer_console.html). Recommend bookmarking this one.
    1. [ ] [Review the Customer flow](https://about.gitlab.com/handbook/business-ops/enterprise-applications/portal/#customer-flow) and [System integrations](https://about.gitlab.com/handbook/business-ops/enterprise-applications/portal/#system-integrations) with focus on the different systems that communication with the customers portal.
1. [ ] Review the [customers portal database structure](https://gitlab.com/gitlab-org/customers-gitlab-com/#database) to get a better understanding of how things are connected.
1. Review these customers portal issues to get a better understanding of what is often asked and how to troubleshoot.
    1. [ ] [Force reassocation](https://gitlab.com/gitlab-com/support/internal-requests/-/issues/1714) using cheatsheet command
    1. [ ] [Delete user account](https://gitlab.com/gitlab-com/support/internal-requests/-/issues/1764)
    1. [ ] [Setting specific values to nil as workaround to bug](https://gitlab.com/gitlab-com/support/internal-requests/-/issues/1749)
    1. [ ] [Rescuing an incomplete order](https://gitlab.com/gitlab-com/support/internal-requests/issues/1339)

### Stage 3: Working on issues

- [ ] **Done with Stage 3**

When taking any action, remember to copy and paste your console commands output to record what's been done.

1. [ ] Pair on internal issues that require console access. Create a pairing session and link it here.
   1. Pairing:

1. [ ] Answer 3 internal issues that require console access and paste the links here. Remember to paste the commands you run (and output if needed) as a comment in the issue.

   1.
   1.
   1.

### Penultimate Stage: Review

Any updates or improvements needed? If there are any dead links, out of date or inaccurate content, missing content whether in this bootcamp or in other documentation, list the needed updates below as tasks for yourself!

- [ ] Update ...

### Final Stage: Completion

1. [ ] Have your manager review this issue.
1. [ ] Manager: schedule a call (or integrate into 1:1) to review how the bootcamp went once you have reviewed this issue.
1. [ ] Once complete, add this bootcamp to the list of training you have completed!

/label ~bootcamp
