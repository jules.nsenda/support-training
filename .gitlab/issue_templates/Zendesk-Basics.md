---
module: Zendesk Basics
area: Helping Customers
level: Beginner
maintainer: atanayno
pathways:
  support-engineer-onboarding:
    position: 6
  support-manager-onboarding:
    position: 5
---

## Introduction

Zendesk is our Support Center and the main communication line with our customers. This module contains some knowledge required to work with Zendesk efficiently as a GitLab Support Engineer.

**Goals of this checklist**

At the end of the checklist you will be able to:
- utilize Zendesk to perform ticket management tasks.
- access Salesforce and the Customer portal to look up customer & account information

**General Timeline and Expectations**

- Read about our [Support Onboarding process](https://about.gitlab.com/handbook/support/training/), the page also shows you the different modules you'll need to complete as part of your Onboarding.
- This issue should take you **1 day to complete**.


### Stage 1: Prerequisites

- [ ] **Done with Stage 1**
<details>
<summary>Tasks</summary>

   Make sure that you have reviewed Zendesk resources listed in the onboarding issue under the section **Initial Zendesk training**:

  1. [ ] Complete Zendesk Agent training
     1. [ ] Sign up at [Zendesk university](https://training.zendesk.com/auth/login?next=%2F).
     You'll receive an email with information on accessing the Zendesk courses
     1. [ ] Complete [Zendesk Overview: Support](https://training.zendesk.com/zendesk-overview-support/) course (approx. 10 min)
  1. [ ] Review additional Zendesk resources
     1. [ ] [UI Overview](https://support.zendesk.com/hc/en-us/articles/203661806-Introduction-to-the-Zendesk-agent-interface)
     1. [ ] [Updating Tickets](https://support.zendesk.com/hc/en-us/articles/212530318-Updating-and-solving-tickets)
     1. [ ] [Working w/ Tickets](https://support.zendesk.com/hc/en-us/articles/203690856-Working-with-tickets)
        *(Read [Avoiding Agent Collisions](https://support.zendesk.com/hc/en-us/articles/203690856-Working-with-tickets#topic_ryy_42g_vt) carefully)*
     1. [ ] [Using Macros](https://support.zendesk.com/hc/en-us/articles/203690796-Using-macros-to-update-tickets)
     1. [ ] [Macros Creation and Editing at GitLab](https://about.gitlab.com/handbook/support/workflows/macros.html)
     1. [ ] [Language Translation using Unbabel](https://about.gitlab.com/handbook/support/workflows/unbabel_translation_in_zendesk.html)
     1. [ ] [Formatting text with Markdown](https://support.zendesk.com/hc/en-us/articles/203691016-Formatting-text-with-Markdown)
     1. [ ] [Marking tickets as spam](https://about.gitlab.com/handbook/support/workflows/marking_tickets_as_spam.html), more details on the [alternate way](https://support.zendesk.com/hc/en-us/articles/203691106-Marking-a-ticket-as-spam-and-suspending-the-requester) to mark tickets as spam.
     1. [ ] Bookmark the [Zendesk Support search reference](https://support.zendesk.com/hc/)
  1. [ ] Your personal Zendesk signature is shown at the end of every ticket response. You can update this to include a personalized valediction like "Thanks" or "Best Regards" by following these steps.
       1. [ ] In Zendesk, click your user icon in the upper-right corner and select View Profile.
       1. [ ] Under Signature in the left sidebar, enter the signature text.
       An example agent signature:
       ```
       {{agent.name}}
       {{agent.role}}
       GitLab, Inc.
       ```
       You can add e.g. `Thanks` or `Best` to your signature, but it's better to always end your message with an applicable and friendly text.

</details>

### Stage 2: Studying ticket lifecycle

- [ ] **Done with Stage 2**
<details>
<summary>Tasks</summary>

  **Note:** we have created a test organization `Zendesk Basics Boot Camp` for this boot camp. Please use it for all the tasks below.

  This section describes common lifecycle of a ticket and explains how to use some Zendesk features while working with tickets.
  Each theoretical part is accompanied by some practical steps. Be sure to go through all the practical steps to make sure that you understand Zendesk behavior
  correctly in each case.

  1. [ ] **Ticket statuses:** read the article [ticket statuses](https://about.gitlab.com/handbook/support/workflows/working-on-tickets.html#understanding-ticket-status)
  to understand how we use them in GitLab Support.

  1. [ ] **Creating new users:** sometimes you may need to create a new user on behalf of a customer. Popular use-case: a customer asks to add somebody to CC but
  this user does not exist in Zendesk. To do it:
     - Point a cursor to `+Add` button and pick `User` in the dropdown.
     - Specify e-mail and the name and click `Add`.
     <br>

     **Practice:**
     - [ ] Create a Zendesk user associated with one of your personal e-mails.
     <br>
  1. [ ] **Creating a new ticket:**

      ##### 1. How customers can create tickets

      GitLab customers can submit ticket using two methods:
      * **Method 1:** By sending an e-mail to `support@gitlab.com`. It is quicker but no details will be provided to GitLab support:
       version information, priority, classification, license info, etc.
      * **Method 2:** Using the support form https://support.gitlab.com/. It is the better way as customer will need to provide some additional information about them
       while submitting the support form.

      ##### 2. How support agents can create tickets

      Sometimes you may need to create a new ticket for a customer. Popular use-cases:
       - Creating a follow-up ticket for emergency.
       - A ticket is too long and it contains a mix of various issues. You want to handle a specific issue separately.
      <br>

     To create a ticket as a support agent:
       - Point a cursor to `+ Add` button and select `Ticket` in the dropdown.
       - Fill the `Requester` field, the CC list, pick the appropriate form, add subject, description.
       <br>

      **Notes:**
        - After support agent creates and submits a ticket as `New` or `Open`, the ticket will not get SLA. SLA is assigned only when customer updates a ticket.
        That said, it is recommended to send a reply to a customer and submit a ticket as `Pending`, `Solved` or `On-hold` depending on the situation.
        - When a ticket is submitted by an unknown user, a user account will be automatically created on Zendesk side.
        <br>

      **Practice:**
      - [ ] Create a ticket as a customer using **Method 1** via the personal e-mail used above. Include some mail formatting to this e-mail and add someone to CC.
      After a while, you will get an automated reply to your personal e-mail.
      - [ ] Find this ticket in `Needs Org & Triage` view in Zendesk, note that it does not have SLA
      - [ ] Note that if you used markdown in your e-mail, it will not be shown as rendered. This is [current Zendesk limitation](https://support.zendesk.com/hc/en-us/community/posts/360038476034-Markdown-for-customers):
      markdown only works for support agents, not for customers.

      <br>
  1. [ ] **Associating user with organization:**
     - Click the username and find the field `Org.` in the list of fields.
     - Paste or start typing the name of the organization. Note that if you copy the organization name from somewhere, it is important to remove leading
     and trailing spaces.
     - If the organization exists in Zendesk, it will be shown in the dropdown. After clicking it, you will see the message `<username> was successfully updated`
     meaning that the user was added to the organization.

     <br>

     **Note:** There is a number of various use cases related to this process, you may get more information in the article
     [Associating needs-org tickets with appropriate organization](https://about.gitlab.com/handbook/support/workflows/associating_needs_org_tickets_with_orgs.html).
     One of the most common cases: suppose that there is a new user on customer’s side, and the customer asks us do add this user to their organization.
       <br>

     - [ ] Learn about [how organizations are created and updated in GitLab's Zendesk](https://about.gitlab.com/handbook/support/workflows/zendesk_organizations_and_users_overview.html).

      **Practice:**
      - [ ] Add previously created user to the organization `Zendesk Basics Boot Camp`. Note that your ticket now has 24h SLA assigned to it.
      as the organization has `Starter` support level assigned.
      <br>
  1. [ ] **Modifying CC list in tickets**. To add yourself, customer or a GitLab team member to CC in the ticket:
     - Open the ticket in the agent interface.
     - Copy the e-mail you need to add and paste it to `CCs` field.
     - After that, click `Submit` button to save changes.
     - Note that clicking submit will also send/save any text that you have entered in the "Public reply"/"Internal note" fields. This is regardless of whether the ticket is submitted as the same state or as a different state.
     - If e-mails of customer staff are added to CC in a ticket, they will only receive public replies while Support Agents added to CC will receive both public and internal replies.
     <br>

     **Note:** At the moment the ability to add CC on customer's side is disabled due to [this security issue](https://gitlab.com/gitlab-com/support/support-team-meta/-/issues/1278),
     that is why we need to do it manually for customers.

     If you need to remove someone from CC:
        - Open the ticket in the agent interface.
        - Click cross near that the e-mail that you need to remove and click `Submit` to save changes.
      <br>

      **Practice:**
      - [ ] Add your work e-mail to CC of the previously created ticket.
      <br>
  1. [ ] **Viewing original e-mail:**

     Sometimes you will see that there is an envelope icon in the upper right part of the customer’s comment.
     It can contain some useful info like:
        - Original formatting of the e-mail message.
        - The list of original recipients that customer attempted to CC.
        - Replies to questions which the customer has placed inline in the quoted text.

     To view original e-mail, follow the steps from [How can I view the original recipients of a ticket I received in Zendesk Support?](https://support.zendesk.com/hc/en-us/articles/231736808-How-can-I-view-the-original-recipients-of-a-ticket-I-received-in-Zendesk-Support-)
      <br>

      **Practice:**
      - [ ] Your ticket has some recipients in CC and some formatting but it is not shown in Zendesk. Open original e-mail to see it.
      <br>
  1. [ ] **Changing ticket's form and sending replies:**

     In most of the cases the ticket form is specified by customers correctly when submitting a ticket but there can be cases when it is not true:
        - It is possible that customer confused it. That said, always review the contents of new tickets and feel free to change the form if you see
        that it is not selected correctly.
        - If a ticket is submitted via e-mail, it will get `Other` value in the form by default. It is important to change the form for such tickets.
        This will make sure that a ticket is routed to the correct view in Zendesk. In majority of cases `Other` form should not be left but there
        could be some exceptions. Usually it happens when the question is not about product e.g. questions about hiring status, invitations to sponsorship,
        conferences, etc.
      <br>

      **Practice:**
      - [ ] Change the ticket's form from `Other` to `Self-managed` or `GitLab.com` depending on your role.
      - [ ] Change `Self-managed Problem Type`  to Other.  As this is a test case we can leave it as 'Other' however when closing regular tickets please choose the correct problem type.
      - [ ] As a support agent, send reply to the ticket and set it to **Pending**.
      - [ ] You will get an update to your personal e-mail. Now reply back from your personal e-mail and check Zendesk - you will see that the ticket is back,
      it has **Open** status and SLA is shown in green.
      <br>
  1. [ ] **Using macros in tickets**

     Macro is a template of a reply that can be useful in a number of standard situations:
        - You need to provide a CE user with information about free support resources.
        - You need to explain GitLab.com user what they can do to reset 2FA.
        - After the call with a customer, you would like to summarize everything that was done during the call.

      It is always great to add your contents to your replies in order to make them better, so it is recommended to use macros as a starting point of your reply,
      not as a final one.

      To add macro to your reply:
       - Open a ticket.
       - Click `Apply Macro` button.
       - Search for the macro if you know its name or find it in the list, click it. The macro will be added to the comment field of your ticket.
      <br>

      **Practice:**

      - [ ] Let's pretend that you scheduled a call with customer. In order not to lose SLA, reply to the ticket and set it to **On-hold**.
      Observe that when it was done, SLA was reset, and the ticket was assigned to you. If you leave it in the **On-hold** status for 4 days,
      it will be reopened but there will be no SLA.
      - [ ] Suppose that the call happened and the issue was fixed. Add **Post customer call** macro to the ticket,
      fill the template provided by this macro and set the ticket's status to **Solved** when sending an update. Check that you got e-mails to your
      personal account regarding all these communications.
      <br>

 1. [ ] **Changing Priority and using macro** To combine a couple of the previous exercises:
      - Read about [ticket priority](https://about.gitlab.com/handbook/support/workflows/setting_ticket_priority.html).

      **Practice:**

      - [ ] Change the priority on your ticket and use the appropriate macro as part of a response.

  1. [ ] **Merging tickets:** sometimes customers may create two duplicate tickets. In order to avoid confusion it is a good idea to merge them into one.
  Let's imagine we have tickets `0001` and `0002`, and we want to merge `0002` into `0001`. To do it:
      - Open the ticket `0002`.
      - Click the arrow in the right part of the ticket next to SLA and select `Merge into another ticket`.
      - `Ticket merge` dialogue will appear. Enter the ticket ID `0001` into the field `Enter ticket ID to merge into` and click `Merge`.
      - In the next window it is important to pay attention to `Requester can see this comment` checkboxes.
      It is recommended to leave it for the ticket `0002` so that the customer will see that the ticket `0002` was merged into `0001`.
      For the ticket `0001` it is better to uncheck this checkbox, otherwise the ticket will lose its SLA, and it will go at the end of the queue
      potentially causing delays in replies.
      - **Important:** note that it is [not possible to unmerge tickets](https://support.zendesk.com/hc/en-us/articles/115004245847-Can-I-un-merge-tickets-),
      be attentive and double check everything before merging tickets.
      <br>

      **Practice:**
      - [ ] Create one more ticket via your personal e-mail, now use the second method i.e. submit it via the support form. After that, as an agent,
      merge the second ticket into the first one.
      <br>

  1. [ ] **Cleanup:** after you are done with all the tests, feel free to close all your tickets:
      - You may mark them as **Solved**. Such tickets will move to **Closed** automatically after 4 days according to
      [Understanding Ticket Status](https://about.gitlab.com/handbook/support/workflows/working-on-tickets.html#understanding-ticket-status).
      - You may mark them as **Pending** but note that if such ticket has SLA, you should send a public reply when moving it to **Pending**.
      After 7 days in **Pending** status, the ticket will be marked as **Solved**.
      - Make sure that there are no test tickets left in **On-hold** status as they will move to **Open** in 4 days.

</details>

### Stage 3: Zendesk Instances

If you are a U.S. Citizen, you will be working in the federal instance. All other team members should be aware that there is a federal instance and who it's for.

- [ ] Read about [the different Zendesk instances](https://about.gitlab.com/handbook/support/workflows/zendesk-instances.html).

### Stage 4: Looking up Customer & Account Information
- [ ] **Done with Stage 4**
<details>
<summary>Click to expand/contract</summary>


We use several tools to look up customer & account information.

**Salesforce**

1. [ ] Familiarize yourself with the workflow for [looking up customer information](https://about.gitlab.com/handbook/support/workflows/looking_up_customer_account_details.html).
1. [ ] Watch the *How to use Salesforce from a support perspective* video linked on the workflow page above.
1. [ ] Familiarize yourself with how to [log in to Salesforce](https://about.gitlab.com/handbook/support/workflows/looking_up_customer_account_details.html#within-salesforce).

**Customer Portal**

1. [ ] Bookmark the [Customer Portal admin page](https://about.gitlab.com/handbook/support/workflows/looking_up_customer_account_details.html#within-customersgitlabcom) and familiarize yourself with how to look up and impersonate customers using the portal. (Impersonate is demonstrated in the Salesforce video above)

**LicenseApp**

1. [ ] Make sure you are able to access the LicenseApp at https://license.gitlab.com using your dev.gitlab.org account. This can be used to search for self-managed licenses and generate them.

</details>

#### Congratulations! You made it, and now have a basic understanding of using Zendesk for ticket management!

You are now ready to continue on your onboarding path to tackle the next module in line based on your role. Reach out to your manager or onboarding buddy if it is not assigned to you yet; you can check the [onboarding page](https://about.gitlab.com/handbook/support/training/) or your `New Support Team Member Start Here` issue to see the list of all modules under the Onboarding pathway!

Please also submit MRs for any improvements that you can think of! The file is located in an issue template in the ['support-training` repository](https://gitlab.com/gitlab-com/support/support-training/blob/master/.gitlab/issue_templates).

/label ~onboarding
