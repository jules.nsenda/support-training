---
module: GitLab.com SAAS Basics
area: Helping Customers
level: Beginner
maintainer: TBD
pathways:
  gitlab-com-saas-support:
    position: 1
---

## Introduction

This checklist provides Support Engineers with the basics of answering GitLab.com (SaaS) product related tickets.

**Goals of this checklist**

At the end of this module, you should be able to:
- use chatops to look up information.
- answer GitLab.com questions.

**General Timeline and Expectations**

- Read about our [Support Onboarding process](https://about.gitlab.com/handbook/support/training/), the page also shows you the modules under the GitLab.com SAAS Support Basics Pathway.
- This issue should take you **2 weeks to complete**.

### Stage 0: Create Your Module

1. [ ] Create an issue using this template by making the Issue Title: <bootcamp title> - <your name>
1. [ ] Add yourself (and your trainer) as the assignees.
1. [ ] Set milestones, if applicable, and a due date to help motivate yourself!

Consider using the Time Tracking functionality so that the estimated length for the bootcamp can be refined.

### Stage 1: Chatops

- [ ] **Done with Stage 1**

Regardless of whether you receive admin access later, you should set up chatops access.

1. [ ] Read over [the Chatops intro and how it works](https://docs.gitlab.com/ee/ci/chatops/README.html).
1. [ ] If you didn't get this as part of onboarding: [Get Chatops access](https://docs.gitlab.com/ee/development/chatops_on_gitlabcom.html#chatops-on-gitlabcom).
    - Note: Use the "Google" sign in on the ops instance to create an account if needed.
1. [ ] Read about [commonly used chatops commands](https://about.gitlab.com/handbook/support/workflows/chatops.html).
1. Practice using chatops by finding your own user and one of the support groups. You can direct message chatops by finding 'GitLab Chatops' under Apps in Slack.
    - [ ] What's your user ID? ID:
    - [ ] What's the group ID for `gitlab-com/support`? ID:
    - [ ] Find a feature flag to search for and get its current status. Take a screenshot and post it in a comment to the issue. Please blur out any user/group-identifiable information or don't include one with scoping unless it's the `gitlab-org` and `gitlab-com` groups.
1. [ ] Take a look at our [improving chatops for Support epic](https://gitlab.com/groups/gitlab-com/-/epics/306). Consider contributing to one of the issues in the future. As more of these functions become available, the more we can move cases out of the admin-only section.

### Stage 2: GitLab.com Accounts

*Note*: Stage 2 and 3 are reversible. For those who want a more "structured" introduction to procedural tickets, start here.
If you're already familiar with GitLab and are used to troubleshooting Self-managed, consider doing Stage 3 first.

See also Stage 4 for a place to record pairing sessions.

- [ ] **Done with Stage 2**

Keep in mind that only GitLab team members are admins, and that accounts are bound by our Terms of Use which each user accepts separately.

_Bottom Line_: Never share information that a user would not have access to even if it's about another user within their organization.
You can still suggest possible solutions based on the description of the problem (and even what you find out about the account), but definitive information cannot be shared.

#### Triaging

Many common tickets are triaged to other places.

1. GDPR. Read _only_ the Overview piece for each workflow so you can see where these should be routed to. There are also macros in ZenDesk for these cases.
    1. [ ] [GDPR Article 15 Requests](https://about.gitlab.com/handbook/support/workflows/gdpr_article-15.html)
    1. [ ] [GDPR Driven Account Deletion](https://about.gitlab.com/handbook/support/workflows/gdpr_account_deletion.html)
1. Subpoenas and other legal (court) requests. See [Subpoenas and other requests for information](https://about.gitlab.com/handbook/support/workflows/information-request.html)
    - Note: If the customer request is _not_ tied to an active court case or other legal matter, it may fall under one of the other cases below.
1. [ ] Violations are typically moved to the Security queue, including
    1. [ ] [Terms of Use or Code of Conduct violations](https://about.gitlab.com/handbook/support/workflows/acceptable_use_violations.html).
        - Note: You can see if an account is blocked via chatops but not the admin notes. Leave the ticket or ask in #support_gitlab-com for help from an admin to check
    1. [ ] [DMCA Removal Requests](https://about.gitlab.com/handbook/support/workflows/dmca.html#submitting-a-dmca-request). Instead of redirecting the user to email the dmca account, the ticket can be moved to the Security queue.
1. [ ] Read over the [other example cases to move to Security](https://about.gitlab.com/handbook/support/workflows/working_with_security.html#identifying-issues-for-transfer-to-security).

#### Email Receiving

One of the main differences of working with GitLab.com is that we receive many requests from [free users](https://about.gitlab.com/support/statement-of-support.html#free-plan-users) for the very reason that we are the administrators of GitLab.com. One of the most common cases is not receiving confirmation, password, or other system notifications from GitLab.

1. [ ] Read the [Confirmation Emails workflow](https://about.gitlab.com/handbook/support/workflows/confirmation_emails.html).
1. [ ] Answer at least 1 ticket with an email receiving workflow: insert ticket link here
1. [ ] Briefly look over [Real Time Blocklist Delisting](https://about.gitlab.com/handbook/support/workflows/rbl_delisting.html) for when GitLab emails are being marked as spam.

#### Other Account Issues

1. [ ] Take a look at the [list of GitLab.com workflows](https://about.gitlab.com/handbook/support/workflows/#GitLab.com) or read the admin only section (Stage 4) and make note what other common cases we take care of, but require admin access.

Remember: When traiging tickets and you're unsure if a user is asking about a Self-managed instance or GitLab.com, use chatops to check if the user exists. The API only searches _primary_ email so in rare cases, someone with console access is required to check if an email has been added as a secondary email. You can ask in #support_gitlab-com or create [a Console Escalation issue](https://gitlab.com/gitlab-com/support/internal-requests/issues/new?issuable_template=GitLab.com%20Console%20Escalation).

### Stage 3: GitLab.com Architecture and Troubleshooting

- [ ] **Done with Stage 3**

As previously noted, GitLab.com users do not have admin accounts. Please do not send any doc links that are self-managed only (check badging at the top of the page or section for **CORE/STARTER/PREMIUM/ULTIMATE ONLY**) or anything from the `administration` section.

#### Architecture

1. Review materials relevant to GitLab.com Architecture
   1. [ ] Read about the [GitLab architecture](https://docs.gitlab.com/ce/development/architecture.html).
   1. [ ] [Production Architecture](https://about.gitlab.com/handbook/engineering/infrastructure/production/architecture/).

#### Searching logs and filing issues

1. Learn about using finding and filing errors. Note: Kibana and Sentry sign in use the dev account.
    1. [ ] [Using Kibana](https://about.gitlab.com/handbook/support/workflows/kibana.html).
        - Note: Typical usage is `rails` and `sidekiq` logs. `HAproxy` is only available to infra team. Ask in the #production Slack channel if necessary.
    1. [ ] [Searching Kibana, Sentry, and filing GitLab issues from Sentry](https://about.gitlab.com/handbook/support/workflows/500_errors.html).
    1. [ ] As an exercise, visit a non-existing page on GitLab.com, such as [gitlab.com/gitlab-com/doesntexist](gitlab.com/gitlab-com/doesntexist). Search Kibana for the 404 error using your username. Add a screenshot of the relevant portion of the error you find in Kibana as a comment to this issue.
    1. [ ] Search Sentry using the `correlation_id`. You may not find anything and the search is not reliable, but take a screenshot of your search and results and add it as a comment to this issue anyway.
    1. [ ] Exercise: use the [import workflow](https://about.gitlab.com/handbook/support/workflows/importing_projects.html#identifying-import-errors) to search for an import error.
    1. [ ] Importing customer projects is the only current case of "getting around" a bug or lack of feature. Read [the rest of the import criteria](https://about.gitlab.com/handbook/support/workflows/importing_projects.html#criteria) to learn when we do these as a courtesy.
1. If an issue can only be resolved through rails console access, you can [file a "GitLab.com Console Escalation" issue](https://about.gitlab.com/handbook/support/workflows/working-with-issues.html#operational-escalation-points).

#### More Troubleshooting

Because GitLab are the stewards of GitLab.com, we sometimes need to troubleshoot some of our integrations. We have workflows for some cases. You don't need to read these in detail now, but keep in mind that these are available:

1. [ ] [Custom domain verification on GitLab.com](https://about.gitlab.com/handbook/support/workflows/verify_pages_domain.html)
1. [ ] [Service Desk Troubleshooting](https://about.gitlab.com/handbook/support/workflows/service_desk_troubleshooting.html)

#### IP blocks and log requests

1. [ ] At times, the infra team blocks users due to alerts of possible abuse. Learn about [sending notices](https://about.gitlab.com/handbook/support/workflows/sending_notices.html).
    - Note: In cases where it's a project endpoint, you may need someone with admin access to look up the project owner. Create an issue and ask an admin to look up the contact information.
1. [ ] Read about [identifying the cause of IP blocks](https://about.gitlab.com/handbook/support/workflows/ip-blocks.html). You won't find a block, but try doing a search by IP of your own IP to see what gets logged in the `rails` log.
1. [ ] When responding to users about possible blocks, you'll need to keep in mind what information you're allowed to provide. See the [log and audit requests workflow](https://about.gitlab.com/handbook/support/workflows/log_requests.html) for more information.

#### Security issues

Security's focus is instance wide security breaches and vulnerabilities. They do however take care of other account related cases (covered in previous stage).

1. Commonly, users often file commit associations as a "security" issue. Follow the [investigate commits](https://about.gitlab.com/handbook/support/workflows/investigate_commits.html) on determining whether it's a security incident.

### Stage 4: Pairings

- [ ] **Done with Stage 4**

If you are doing this bootcamp as part of onboarding, feel free to remove this section, mark it as "Not Applicable" (N/A) or similar.

1. Have 5 pairing sessions specifically geared towards answering GitLab.com tickets or issues.
   1. [ ] with ___; issue link: ___
   1. [ ] with ___; issue link: ___
   1. [ ] with ___; issue link: ___
   1. [ ] with ___; issue link: ___
   1. [ ] with ___; issue link: ___
1. [ ] Check the "Dotcom Support Sync" meeting doc for recent updates/issues (linked from calendar entry), and consider attending when in your timezone (3 week rotation). Otherwise, check and add to the doc regularly.
1. [ ] Consider adding yourself to the every 6 weeks cross-region GitLab.com crush sessions (check Support calendar).
1. If you are doing Stage 5 (admin access), do 2 more focusing on tickets or issues that require admin access.
   1. [ ] with ___; issue link: ___
   1. [ ] with ___; issue link: ___

### Stage 5: GitLab.com admin access (with manager approval)

- [ ] **Done with Stage 5**

1. [ ] Typically at week 3-4, you and your manager should check in that you're ready for admin access. Submit a [Single Person Access Request](https://about.gitlab.com/handbook/business-ops/employee-enablement/it-ops-team/access-requests/#single-person-access-request) for admin access and to be added to the `@support-dotcom` Slack group.
1. [ ] Have an existing Owner add you to the `gitlab-com/support/dotcom` group as an `Owner` so that you are a direct member.

#### Internal Requests

1. Update notification settings to **Watch** which will send you notifications for all activity, or **Custom** for new issues in the following projects:
    1. [ ] [internal requests](https://gitlab.com/gitlab-com/support/internal-requests). Alternatively, given there are a lot of different requests, only subscribe to the following labels:
        - [Admin Escalation](https://gitlab.com/gitlab-com/support/internal-requests/-/issues?label_name%5B%5D=Admin+Escalation)
        - [DEWR](https://gitlab.com/gitlab-com/support/internal-requests/-/issues?label_name%5B%5D=DEWR) (Dotcom Escalation Weekly Report)
    1. [ ] [GDPR](https://gitlab.com/gitlab-com/gdpr-request)
1. When you see a GDPR request, follow the appropriate workflow below to fulfill the request and link the issue that you've done.
    1. [ ] [GDPR Article 15 Requests](https://about.gitlab.com/handbook/support/workflows/gdpr_article-15.html)
        1. [ ] Link to issue:
    1. [ ] [GDPR Driven Account Deletion](https://about.gitlab.com/handbook/support/workflows/gdpr_account_deletion.html)
        1. [ ] Link to issue:
1. For [internal requests](https://gitlab.com/gitlab-com/support/internal-requests) requests, you only need to work on the requests that related specifically to GitLab.com. Many of the other issues as labelled are specific to other training (such as Console or Licensing).
    1. [ ] Check out the list of [templates](https://gitlab.com/gitlab-com/support/internal-requests/-/tree/master/.gitlab/issue_templates).
    1. [ ] Read the [Servicing Internal Requests workflow](https://about.gitlab.com/handbook/support/workflows/internal_requests.html) which covers most internal issues. Link a couple that you've completed here.
        1. [ ] Link to issue:
        1. [ ] Link to issue:

#### User Accounts

Aside from the ones covered in Stage 2, the most common user requests have to do with disabling 2FA and dormant namespace requests. However, most of these workflow apply in other cases as well.

1. [ ] When making any changes to a user account, make sure to [use an admin note](https://about.gitlab.com/handbook/support/workflows/admin_note.html).
1. [ ] For all 2FA disabling requests, use the [Account Ownership Verification](https://about.gitlab.com/handbook/support/workflows/account_verification.html). This workflow may be used for some other cases as well where we want to verify the ownership.
1. [ ] [Dormant Namespace Requests](https://about.gitlab.com/handbook/support/workflows/dormant_username_policy.html) apply to groups and users. Included here since it is often for users.
1. [ ] Except in the cases above, or wherever else there is a clear ask of the action, always [ask for permission to take action]().

#### Other Common Requests

1. [ ] reCaptcha complaints are often due to actions on a specific issue by a specific user being marked as spam. Learn how to [mark as ham](https://about.gitlab.com/handbook/support/workflows/managing_spam.html#false-positives-ham).
1. [ ] Silver+ customers can request to immediately delete a project if their Group has [Enable delayed project removal](https://docs.gitlab.com/ee/user/group/#enabling-delayed-project-removal-premium) configured. Learn to [Delete Projects in Soft-Deleted State](https://about.gitlab.com/handbook/support/workflows/hard_delete_project.html). Note that this means that all Bronze customers will have their projects removed immediately by default.
1. [ ] [Reinstating blocked accounts](https://about.gitlab.com/handbook/support/workflows/reinstating-blocked-accounts.html) should also be confirmed with the abuse team first. In cases of violations, you can consider moving to the Security queue for them to respond directly.
1. [ ] [Personal Identifiable Information Removal Requests](https://about.gitlab.com/handbook/support/workflows/pii_removal_requests.html) are uncommon, but read the workflow to understand Support's responsibility.

### Stage 6: GitLab.com console (with manager approval)

Depending on your prior experience and readiness, open the bootcamp listed below. Your manager may decide to have you pick this up later once you have more familiarity with GitLab.

1. [ ] Open a bootcamp on [GitLab.com Console](/gitlab-com/support/support-training/issues/new?issuable_template=Bootcamp%20-%20GitLab.com%20Console).

## Congratulations! You made it, and can now help customers with GitLab.com SAAS product related tickets.

Please also submit MRs for any improvements that you can think of! The file is located in an issue template in the ['support-training` repository](https://gitlab.com/gitlab-com/support/support-training/blob/master/.gitlab/issue_templates).

/label ~module
